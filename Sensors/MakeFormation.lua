local sensorInfo = {
	name = "MakeFormation",
	desc = "Returns relative positions for others based on given direction.",
	author = "novotnyt",
	date = "2018-05-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo

-- @description return current wind statistics
return function(direction)       
  local positions = {}
  local row = 0
  local verticalUnit = {-direction.dirX*3, direction.dirY*3, -direction.dirZ*3}
  local horizontalUnit = {direction.dirZ*3, direction.dirY*3, -direction.dirX*3}
  local function GetRelative(mulVertical, mulHorizontal) 
    return Vec3(verticalUnit[1]*mulVertical + horizontalUnit[1]*mulHorizontal,
                verticalUnit[2]*mulVertical + horizontalUnit[2]*mulHorizontal,
                verticalUnit[3]*mulVertical + horizontalUnit[3]*mulHorizontal
               )
  end
  --spEcho(#units)
  for i = 1, #units do
    --creates rows of width 7 where units are distributed symmetrically around center
    positions[i] = GetRelative(math.floor((i-1)/7), math.floor(((i-1)%7+1)/2)*(2*(i%2)-1)) 
  end
	  return positions
end