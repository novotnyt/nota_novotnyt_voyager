local sensorInfo = {
	name = "WindDirection",
	desc = "Return data of actual wind.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = spGetWind() 
	if (Script.LuaUI('arrow')) then
    --call drawing of wind direction into the GUI
    --Spring.Echo(z)
    local unitID = units[1]
		Script.LuaUI.arrow(
			unitID, -- key
			{	-- data
				direction = Vec3(dirX,dirY,dirZ)
			}
		)
  end
	return {
		dirX = dirX,
		dirY = dirY,
		dirZ = dirZ,
		strength = strength,
		normDirX = normDirX,
		normDirY = normDirY,
		normDirZ = normDirZ,
	}
end