local sensorInfo = {
	name = "CommanderPosition",
	desc = "Return data of position of first commander. If there is none, returns nil",
	author = "novotnyt",
	date = "2018-05-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spGetUnitPosition = Spring.GetUnitPosition

-- @description return current wind statistics
return function()
	if #units > 0 then
    posX, posY, posZ = spGetUnitPosition(units[1])
    return Vec3(posX, posY, posZ)
  end
  return nil
end